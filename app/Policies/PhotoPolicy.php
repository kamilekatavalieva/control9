<?php

namespace App\Policies;

use App\Models\Photo;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class PhotoPolicy
{
    use HandlesAuthorization;


    /**
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        if(Auth::check()) {
            return $user->id == Auth::user()->id;
        }
        return false;
    }


    /**
     * @param User $user
     * @param Photo $photo
     * @return bool
     */
    public function delete(User $user, Photo $photo)
    {
        return $user->id == $photo->user_id;
    }


}
