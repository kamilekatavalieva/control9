<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return [
            'content' => ['bail','required', 'regex:/([^(<)(>)])$/u', 'max:2048', 'string', 'min:5'],
            'grade' => ['bail','required', 'in:1,2,3,4,5', 'numeric'],
        ];
    }
}
