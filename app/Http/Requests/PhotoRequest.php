<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return [
            'denomination' =>['bail','required', 'between:3,250', 'regex:/([^(<)(>)])$/u',] ,
            'photo' => ['bail', 'required', 'mimes:jpg,bmp,png'],
        ];
    }
}
