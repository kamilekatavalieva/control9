<?php

namespace App\Http\Form;

use App\Models\User;
use Illuminate\Http\Request;

abstract class UserForm
{
    /**
     * @param Request $request
     * @param User $user
     * @return mixed
     */
    public static function perform(Request $request, User $user)
    {
        return(new static)->editing($request, $user);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return mixed
     */
    protected abstract function editing(Request $request, User $user);
}
