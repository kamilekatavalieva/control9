<?php

namespace App\Http\Form;

use App\Models\User;
use Illuminate\Http\Request;

class UserActionsForm extends UserForm
{

    /**
     * @inheritDoc
     */
    protected function editing(Request $request, User $user)
    {
        $data = $request->all();
        $user->update($data);
        return $user;
    }
}
