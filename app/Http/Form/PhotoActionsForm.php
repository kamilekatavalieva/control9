<?php

namespace App\Http\Form;

use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PhotoActionsForm extends PhotoForm
{

    /**
     * @inheritDoc
     */
    protected function handle(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        if ($request->hasFile('photo')){
            $generator = config('platform.attachment.generator');
            $engine = new $generator($request->file('photo'));
            $storage = Storage::disk(config('platform.attachment.disk'));
            if (! $storage->has($engine->path())){
                $storage->makeDirectory($engine->path());
            }
            $path = $request->file('photo')->store($engine->path(), 'public');
            $data['photo'] = '/storage/' . $path;
        }
        return  Photo::create($data);
    }
}
