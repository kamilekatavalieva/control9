<?php

namespace App\Http\Controllers;

use App\Http\Form\UserActionsForm;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UsersController extends Controller
{


    /**
     * @param User $user
     * @return Application|Factory|View
     */
    public function show(User $user)
    {
        return view('clients.users.show', compact('user'));
    }


    /**
     * @param User $user
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('edit', $user);
        return view('clients.users.edit', compact('user'));
    }


    /**
     * @param UserRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UserRequest $request, User $user):RedirectResponse
    {
        $user = UserActionsForm::perform($request, $user);
        return redirect()->route( 'users.show', ['user' => $user]);

    }

}
