<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Photo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    /**
     * @param CommentRequest $request
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function store(CommentRequest $request, Photo $photo):RedirectResponse
    {
        $data = $request->validated();
        $data['user_id'] = auth()->user()->id;
        $data['photo_id'] = $photo->id;

        Comment::create($data);

        return redirect()->route('photos.show', $photo->id);
    }

}
