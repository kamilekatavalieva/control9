<?php

namespace App\Http\Controllers;

use App\Http\Form\PhotoActionsForm;
use App\Http\Requests\PhotoRequest;
use App\Models\Photo;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PhotosController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $photos = Photo::latest()->paginate(8);
        return view('clients.photos.index', compact('photos'));
    }


    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Photo::class);
        return view('clients.photos.create');
    }


    /**
     * @param PhotoRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(PhotoRequest $request):RedirectResponse
    {
        $this->authorize('create', Photo::class);
        $photo = PhotoActionsForm::execute($request);
        $photo->save();
        return redirect()->route('users.show', $photo->user->id);

    }


    /**
     * @param Photo $photo
     * @return Application|Factory|View
     */
    public function show(Photo $photo)
    {
        $comments = $photo->comments()->orderByDesc('updated_at')->paginate(5);
        if($photo->comments->count() != 0) {
            $grade = $photo->comments()->avg('grade');
        } else {
            $grade = 0;
        }
        return view('clients.photos.show', compact('photo', 'grade', 'comments'));
    }


    /**
     * @param Photo $photo
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Photo $photo):RedirectResponse
    {
        $this->authorize('delete', $photo);
        $photo->delete();
        return redirect()->route('users.show', $photo->user_id);

    }
}
