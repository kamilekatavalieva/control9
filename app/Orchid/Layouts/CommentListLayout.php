<?php

namespace App\Orchid\Layouts;

use App\Models\Comment;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return[
            TD::make('content', 'Content')
                ->render(function (Comment $comment) {
                    return Link::make($comment->content)
                        ->route('platform.comments.edit', $comment);
                }),
            TD::make('grade', 'Grade')
                ->render(function (Comment $comment) {
                    return Link::make($comment->grade)
                        ->route('platform.comments.edit', $comment);
                }),

            TD::make('created_at', 'Created'),
            TD::make('updated_at', 'Last updated'),
        ];
    }
}
