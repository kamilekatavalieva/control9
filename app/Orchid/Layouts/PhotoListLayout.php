<?php

namespace App\Orchid\Layouts;

use App\Models\Photo;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PhotoListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'photos';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('denomination', 'Denomination')
                ->render(function (Photo $photo) {
                    return Link::make($photo->denomination)->route('platform.photos.edit', $photo);
                }),
            TD::make('created_at', 'Created'),
            TD::make('updated_at', 'Last updated')
        ];
    }
}
