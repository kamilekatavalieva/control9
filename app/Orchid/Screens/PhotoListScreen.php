<?php

namespace App\Orchid\Screens;

use App\Models\Photo;
use App\Orchid\Layouts\PhotoListLayout;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class PhotoListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Photos';


    /**
     * @var string
     */
    public $description = "Photos";

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'photos' => Photo::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new')
                ->icon('icon-plus')
                ->route('platform.photos.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            PhotoListLayout::class
        ];
    }
}
