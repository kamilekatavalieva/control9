<?php

namespace App\Orchid\Screens;

use App\Models\Comment;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create photo';


    /**
     * @var string
     */
    public $description = 'Create comment screen';


    /**
     * @var bool
     */
    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Comment $comment): array
    {
        $this->exists = $comment->exists;
        if ($this->exists) {
            $this->name = "Edit comment";
            $this->description = "Edit comment description";
        }
        return [
            'comment' => $comment,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create comment')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('comment.content')
                    ->title('Content')
                    ->placeholder('Title placeholder...')
                    ->help('Help message')
                    ->required(),


                Relation::make('comment.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id')
                    ->required(),

                Relation::make('comment.photo_id')
                    ->title('Photo')
                    ->fromModel(Photo::class, 'denomination', 'id')
                    ->required(),

                Select::make('comment.grade')
                    ->title('Grade')
                    ->options([
                        1  => 1,
                        2  => 2,
                        3  => 3,
                        4  => 4,
                        5  => 5,
                    ])
                    ->required(),
            ])

        ];
    }


    /**
     * @param Comment $comment
     * @param Request $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Comment $comment, Request $request): RedirectResponse
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('Your successfully created comment');
        return redirect()->route('platform.comments.list');
    }


    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function remove(Comment $comment):RedirectResponse
    {
        $comment->delete()
            ? Alert::info('Comment successfully deleted.') : Alert::error('Error!');
        return redirect()->route('platform.comments.list');

    }
}
