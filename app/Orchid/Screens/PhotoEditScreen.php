<?php

namespace App\Orchid\Screens;

use App\Models\Photo;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Relation;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class PhotoEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create photo';

    /**
     * @var string
     */
    public $description = 'Create photo screen';

    /**
     * @var bool
     */
    public $exists = false;

    /**
     * @param Photo $photo
     * @return Photo[]
     */
    public function query(Photo $photo): array
    {
        $this->exists = $photo->exists;
        if ($this->exists) {
            $this->name = "Edit photo";
            $this->description = "Edit photo description";
        }
        return [
            'photo' => $photo,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create photo')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }


    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('photo.denomination')
                    ->title('Denomination')
                    ->placeholder('Title placeholder...')
                    ->help('Help message')
                    ->required(),
                Picture::make('photo.photo')
                    ->targetRelativeUrl()
                    ->title('Content photo')
                    ->required(),
                Relation::make('photo.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id')
                    ->required(),
            ])
        ];
    }

    /**
     * @param Photo $photo
     * @param Request $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Photo $photo, Request $request): RedirectResponse
    {
        $photo->fill($request->get('photo'))->save();
        Alert::info('Your successfully created photo');
        return redirect()->route('platform.photos.list');
    }

    /**
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function remove(Photo $photo): RedirectResponse
    {
        $photo->delete() ? Alert::info('Photo successfully deleted') : Alert::error('Error!');
        return redirect()->route('platform.photos.list');
    }
}
