<?php

use App\Http\Controllers\CommentsController;
use App\Http\Controllers\LanguageSwitcherController;
use App\Http\Controllers\PhotosController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('language')->group(function() {
    Route::get('/', [PhotosController::class, 'index'])->name('home');
    Route::resource('photos', PhotosController::class)->except(['edit', 'update']);
    Route::resource('users', UsersController::class)->only(['show', 'edit', 'update']);
    Route::resource('photos.comments', CommentsController::class)->only(['store',])
        ->middleware('auth');


    Auth::routes();

});
Route::get('language/{locale}', [LanguageSwitcherController::class, 'switcher'])->name('language.switcher')
    ->where('locale', 'en|ru');
