@extends('layouts.app')
@section('content')
    <div>
        <h3>@lang('messages.create_new_photo')</h3>
    </div>
    <form enctype="multipart/form-data" method="post" action="{{route('photos.store')}}">
        @csrf
        <div class="form-group mt-3">
            <label for="denomination">@lang('messages.denomination')</label>
            <input type="text"
                   class="form-control border-success  @error('denomination') is-invalid border-danger @enderror"
                   id="denomination" name="denomination">
        </div>
        @error('denomination')
        <p class="text-danger">{{ $message }}</p>
        @enderror
        <div class="form-group mt-3">
            <div class="custom-file">
                <label for="customFile">@lang('messages.choose_file')</label>
                <input type="file" class="custom-file-input @error('photo') is-invalid border-danger @enderror"
                       id="customFile" name="photo">
                <label class="custom-file-label" for="customFile">@lang('messages.choose_file')</label>
            </div>
        </div>
        @error('photo')
        <p class="text-danger">{{ $message }}</p>
        @enderror
        <button type="submit" class="btn btn-outline-primary">@lang('messages.save')</button>
    </form>
@endsection
