@extends('layouts.app')
@section('content')

    <div class="row gallery-list">
        @foreach($photos as $photo)
            <div class="col-md-3 gallery-list--item" style="padding: 35px 0 0 0;">
                <div class="card" style="width: 18rem; height: 100%">
                    <div class="img-wrapper">
                        <a href="{{route('photos.show', ['photo' => $photo])}}">
                            <img src="{{asset($photo->photo)}}" class="card-img"
                                 alt="{{asset($photo->photo)}}" height="100%">
                        </a>
                    </div>

                    <div class="card-body d-flex flex-column">
                        <h5 class="card-title"><b>{{$photo->denomination}}</b></h5>
                    </div>

                    <div class="card-footer">
                        @lang('messages.author'): <a
                            href="{{route('users.show', ['user' => $photo->user])}}">{{$photo->user->name}}</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $photos->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection
