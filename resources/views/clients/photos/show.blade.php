@extends('layouts.app')
@section('content')

    <div>
        <h3><b>{{$photo->denomination}}</b></h3>
        <div class="card" style="width: 18rem;">
            <img src="{{asset($photo->photo)}}" class="card-img-one"
                 alt="{{asset($photo->photo)}}">
        </div>
    </div>

    <div>
        <p>@lang('messages.average_grade'): {{round($grade, 2)}}</p>
        @if($comments->count() > 0)
            <div class="list-group">
                @foreach($photo->comments as $comment)
                    <div class="list-group-item list-group-item-action shadow-lg">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="w-75">
                                <h5 class="mb-1">
                                    <a href="" class="text-decoration-none">{{ $comment->user->name }}</a>
                                </h5>
                                <p class="mb-1"><em>{{ $comment->content }}</em></p>
                                <span
                                    class="g-color-gray-dark-v4 g-font-size-12"><em>@lang('messages.adding'):{{$comment->created_at->diffForHumans()}}</em></span>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="row justify-content-md-center mt-5">
                    <div class="col-md-auto">
                        {{ $comments->withQueryString()->links('pagination::bootstrap-4') }}
                    </div>
                </div>
                @endif
                @if(Auth::user() != $photo->user)
                    <h3 class="mt-5 mb-3"> @lang('messages.add_new_comment')</h3>
                    <form action="{{ route('photos.comments.store', $photo) }}" method="POST">
                        @csrf
                        <div class="input-group">
                            <label for="grade" style="margin-right: 5px;">@lang('messages.grade')</label>
                            <select class="form-control border-info  @error('grade') is-invalid border-danger @enderror"
                                    name="grade">
                                <option value="0" selected></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            @error('grade')
                            <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="input-group">
                        <textarea class="form-control border-info @error('content') is-invalid border-danger @enderror"
                                  name="content" aria-label="With textarea"
                                  cols="30" rows="5"></textarea>
                            </div>
                            @error('content')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="row mt-2 mb-5">
                            <div class="col-12">
                                <button type="submit"
                                        class="btn btn-outline-info"> @lang('messages.add_new_comment')</button>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
@endsection







