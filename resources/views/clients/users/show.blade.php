@extends('layouts.app')
@section('content')
    <h2><b>@lang('messages.prof')</b> {{$user->name}}
        @if($user->can('edit', App\Models\User::class))
            <a href="{{route('users.edit', ['user'=> $user])}}" class="btn btn-primary btn-sm active" role="button"
               aria-pressed="true">@lang('messages.edit')</a>
        @endcan
        @if($user->can('create', App\Models\Photo::class))
            <a href="{{route('photos.create')}}" class="btn btn-primary btn-sm active" role="button"
               aria-pressed="true">@lang('messages.create_new_photo')</a>
        @endif
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">@lang('messages.photo')</th>
                <th scope="col">@lang('messages.denomination')</th>
                <th scope="col">@lang('messages.action')</th>
            </tr>
            </thead>
            <tbody>

            @foreach($user->photos as $photo)
                <tr>
                    <td>
                        <a href="{{route('photos.show', ['photo' => $photo])}}">
                            <img src="{{asset($photo->photo)}}" class="card-img-top"
                                 alt="{{asset($photo->photo)}}" width="50" height="100%">
                        </a>
                    </td>
                    <td>{{$photo->denomination}}</td>
                    <td>
                        @can('delete', $photo)
                            <form style="float: left; margin-right: 12px;"
                                  action="{{route('photos.destroy', ['photo' => $photo])}}" method="post">
                                @method('delete')
                                @csrf
                                <button class="btn btn-danger btn-sm active ">@lang('messages.remove')</button>
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
@endsection


