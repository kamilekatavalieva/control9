<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::all();
        $photos = \App\Models\Photo::all();
        for($i = 0; $i < 100; $i++) {
            \App\Models\Comment::factory()
                ->for($users->random())
                ->for($photos->random())
                ->create();
        }
    }
}
