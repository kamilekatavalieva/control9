<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::factory(5)->create();
        $users->each(function($user) {
            \App\Models\Photo::factory()->count(rand(3, 20))->for($user)->create();
        });
        $this->call(CommentSeeder::class);
    }
}
