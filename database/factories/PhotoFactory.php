<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PhotoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        return [
            'denomination' => $this->faker->paragraph(1),
            'photo' => $this->getImage(rand(1, 20)),
        ];
    }

    /**
     * @param int $image_number
     * @return string
     */
    private function getImage($image_number = 1): string
    {
        $path = storage_path() . "/photo_pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put(date('Y/m/d', time()) . "/". $image_name, $resize->__toString());
        return '/storage/' . date('Y/m/d', time()) . '/'.$image_name;

    }
}
