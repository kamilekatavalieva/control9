<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        return [
            'content' => $this->faker->sentence(4),
            'grade' => rand(1, 5),
            'photo_id' => rand(1, 20),
            'user_id' => rand(1, 5)
        ];
    }
}
